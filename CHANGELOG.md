## Change log

### 1.18.1 (20220419)
* Change register API url `https://api.gtc.beta.glycosmos.org/glycan/register` to `https://api.beta.glytoucan.org/glycan/register`

### 1.18.0 (20220418)
* Change API url `https://gtc.beta.glycosmos.org/` to `https://beta.glytoucan.org/`

### 1.17.1 (20220325)
* Change anomeric state of the reducing end of Glc3Man9GlcNAc2 (α -> β) 

### 1.17.0 (20220208)
* Add "Write" function (for GlycoWorkbench)
* Change usable undo count 50 from 20
* Change configuration of N-glycan high mannose (Man6 -> Man9)

### 1.16.0 (20220204)
* Implemented function and user-interface of GlyTouCan registration for GlycoWorkbench
  * Send Structure Data
  * GlyTouCanID List
  * Change User

### 1.15.0 (20220111)
* Bug fix (#30, #35, #36)
* GIC develop phase-2
* Add N-Glycan templates: Complex, Hybrid, Oligosaccharide
* Display an available file format in file select dialog
* Fixed reducing end of O-glycan (core1~8)

### 1.14.0 (20211201)
* Update maven configurations

### 1.13.0 (20210805)
* 


### 1.12.0 ()

### 1.11.0 ()

### 1.10.0 ()

### 1.0.91 ()

### 1.0.9 ()

### 1.0.8 ()

### 1.0.7 ()

### 1.0.6 ()

### 1.0.5-snapshot ()

### 1.0.4-snapshot ()